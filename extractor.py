from bs4 import BeautifulSoup
import requests

HOME_CRIMEA='http://crimea.gov.ru'
NEWS_CRIMEA='http://crimea.gov.ru/news'

NEWS_RK='https://rk.gov.ru/ru/article/view-all'
HOME_RK='https://rk.gov.ru/'

def get_text_for_rk(link):
    resp=requests.get(link)
    soup = BeautifulSoup(resp.text)
    info=soup('div', attrs={'class': 'info'})
    return info[0].text

def get_text_for_crimea_govno(link):
    resp=requests.get(link)
    soup = BeautifulSoup(resp.text)
    info=soup('div', attrs={'class': 'news_content'})
    return info[0].text

def get_links_for_crimea_govno():
    resp=requests.get(NEWS_CRIMEA)
    news=[]
    soup = BeautifulSoup(resp.text)

    all_news=soup.findAll("div", class_="news_content")
    for new in all_news:
        date=new.find("span", class_="date").text
        title=new.find("h2").text
        link=HOME_CRIMEA+str(new.find("a").attrs['href'])
        news.append({link, title, date})
    return news

def get_links_for_rk():
    resp=requests.get(NEWS_RK)
    news=[]
    soup = BeautifulSoup(resp.text)

    all_news=soup.findAll("div", class_="news-box")
    for new in all_news:
        date=new.find("div", class_="news-box__date").text
        title=new.find("div", class_="news-box__content").text
        link= HOME_RK+str(new.find("a").attrs['href'])
        news.append({link, title, date})
    return news

if __name__=="__main__":
    
    print(get_links_for_crimea_govno())
    print(get_links_for_rk())

    # link1='https://rk.gov.ru/ru/article/show/5489'#rk
    # print(get_text_for_rk(link1))
    # link2='http://crimea.gov.ru/news/01_09_2018_3'#crimea
    # print(get_text_for_crimea_govno(link2))
