import re

def regex_search(article_text):
    '''function returns count of matches'''
    #regex list
    regex_list=[]
    with open ("data/1_new_regex.txt", "r") as myfile:
        regex_list=myfile.read().splitlines()
    print(regex_list)
    #counting matches
    article_count=0
    for i in regex_list:
        count=0
        regex=re.compile(i)
        for match in regex.finditer(article_text):
            s = match.start()
            e = match.end()
            print('String match "%s" at %d:%d' % (article_text[s:e], s, e))
            count=count+1
            article_count=article_count+1
        print('String match "%d" times %s' % (count,i))
    print('Strings matched "%d" timess at all article' % (article_count))
    return article_count

if __name__=="__main__":
    #target
    
    import sys
    import extractor as e

    
    
    link1='https://rk.gov.ru/ru/article/show/5489'#rk
    link2='http://crimea.gov.ru/news/01_09_2018_3'#crimea

    print(regex_search(e.get_text_for_rk(link1)))
    print(regex_search(e.get_text_for_crimea_govno(link2)))


